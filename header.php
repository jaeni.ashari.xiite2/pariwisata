<?php require_once "koneksi.php"; ?>

<?php $login = false;
if (isset($_SESSION['nama'])) {
    $login = true;
}
?>



<!DOCTYPE html>
<html lang="en-us">
<meta charset="utf-8">

<head>
    <title>Pariwisata Kota Tangerang Selatan</title>
    <link rel="stylesheet" href="style.css">
    <link href="css/jasny-bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <div class="top">
            <p id="judul_web">Pariwisata Kota Tangerang Selatan</p>
        </div>
        <div>
            <header>
                <ul>
                    <li><a href=" index.php">Home</a></li>
                    <li><a href="tampil_komen.php"=>Discuss Forum</a></li>
                    <li><a href="tampil_foto.php">Gallery</a></li>
                    <li><a href="tampil_event.php">Event</a></li>
                    <?php if ($login == true) : ?>
                        <li><a href="tambah_data.php">Tambah Data</a></li>
                        <li><a href="approval.php">Approval</a></li>
                    <?php endif; ?>
                    <?php if ($login == true) : ?>
                        <li><a href="logout.php">Logout</a></li>
                    <?php else : ?>
                        <li><a href="login.php">Login</a></li>
                    <?php endif; ?>
                </ul>
            </header>
        </div>
    </div>

</html>