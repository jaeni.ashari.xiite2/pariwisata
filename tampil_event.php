<?php
require_once "header.php";
require_once "koneksi.php";
?>

<?php
$login = false;
if (isset($_SESSION['nama'])) {
    $login = true;
    if (cek_status($_SESSION['nama']) == 1) {
        $super_user = true;
    }
}
?>
<?php $artikel = tampilkan_event(); ?>

<?php

if (isset($_GET['search'])) {
    $cari  = $_GET['search'];
    $artikel  = hasil_cari($cari);
}
?>
<div class="wrapper">
    <nav>
        <form action="" method="get">
            <input class="search" type="search" name="search" placeholder="Search......">
        </form>
    </nav>
    <div class="marquee">
        <marquee>
            <p id="teks_berjalan">Destinasi Pariwisata Kota Tangerang Selatan Sudah dikenal para wisatawan luar Kota. Menurut Dinas Pariwisata Kota Tangerang Selatan Mengatakan, wisatawan yang berkunjung ke Tangerang Selatan pada tahun 2019 telah mencapai sekitar 4.082 wisatawan luar provinsi </p>
        </marquee>
    </div>
    <div class="form2">
        <?php if ($login == true) : ?>
            <p><a class="btn btn-success" href="event.php"> (+) Tambah Event </a></p>
        <?php endif; ?>
        <p id="judul_form">Event yang akan datang</p>
        <?php while ($row = mysqli_fetch_assoc($artikel)) : ?>
            <div class="artikel2">

                <h2><a href="detail_event.php?id=<?= $row['id']; ?>"><?= $row['topik']; ?></a></h2>
                <p id="detail"><?= excerpt($row['detail']); ?></p>
                <p id="terkait">terkait : <?= $row['terkait']; ?></p>
                <p id="publish">publish : <?= $row['publish']; ?></p>

                <?php if ($login == true) : ?>
                    <a class="btn btn-primary" href="edit_event.php?id=<?= $row['id']; ?>">Edit </a>

                    <a class="btn btn-danger" href="hapus_event.php?id=<?= $row['id']; ?>"> Hapus</a>

                <?php endif; ?>
            </div>
        <?php endwhile; ?>
    </div>
    <?php require_once "sidebar.php"; ?>
</div>


<?php require_once "footer.php";
