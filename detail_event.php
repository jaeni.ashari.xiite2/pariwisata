<?php require_once "header.php";
require_once "koneksi.php";

$id = $_GET['id'];

if (isset($_GET['id'])) {
    $artikel2 = detail_event($id);

    while ($row = mysqli_fetch_assoc($artikel2)) {
        $topik = $row['topik'];
        $detail   = $row['detail'];
        $terkait   = $row['terkait'];
        $publish = $row['publish'];
    }
}


?>
<style media="screen">
    .form_tambah {
        width: 80%;
        height: 800px;
        background: white;
        float: left;
        padding: 30px;
        box-sizing: border-box;
        border-bottom: 0.5px solid #dddddd;
    }

    #judul_single {
        font-size: 25px;
        font-weight: bold;
    }

    #isi_single {
        font-size: 15px;
        margin-top: 20px;
    }

    #isi {
        font-size: 12px;
    }
</style>

<div class="wrapper">
    <nav>
        <form action="" method="get">
            <input class="search" type="search" name="search" placeholder="Search......">
        </form>
    </nav>
    <div class="marquee">
        <marquee>
            <p id="teks_berjalan">Destinasi Pariwisata Kota Tangerang Selatan Sudah dikenal para wisatawan luar Kota. Menurut Dinas Pariwisata Kota Tangerang Selatan Mengatakan, wisatawan yang berkunjung ke Tangerang Selatan pada tahun 2019 telah mencapai sekitar 4.082 wisatawan luar provinsi </p>
        </marquee>
    </div>
    <div class="form2">
        <div class="form_tambah">
            <p id="judul_single"><?= $topik; ?></p><br>
            <p id="isi_single">Tanggal Publish : <?= $publish; ?></p><br>
            <p id="isi_single"><?= $detail; ?></p><br>
            <p id="tag_awal"><?= $terkait; ?></p><br>
        </div>
    </div>
    <?php require_once "sidebar.php"; ?>
</div>
<?php require_once "footer.php"; ?>