<?php
include "includes/config.php";
include "header.php";
?>

<body>
	<div class="wrapper">
		<nav>
			<form action="" method="get">
				<input class="search" type="search" name="search" placeholder="Search......">
			</form>
		</nav>
		<div class="marquee">
			<marquee>
				<p id="teks_berjalan">Destinasi Pariwisata Kota Tangerang Selatan Sudah dikenal para wisatawan luar Kota. Menurut Dinas Pariwisata Kota Tangerang Selatan Mengatakan, wisatawan yang berkunjung ke Tangerang Selatan pada tahun 2019 telah mencapai sekitar 4.082 wisatawan luar provinsi </p>
			</marquee>
		</div>
		<div class="form2">
			<div class="container body">
				<h1>Upload Gambar</h1>
				<form method="post" action="upload-proses.php" enctype="multipart/form-data">
					<div class="fileinput fileinput-new" data-provides="fileinput">
						<div class="fileinput-new thumbnail" style="width: 700px; height: 150px;">
							<img data-src="holder.js/100%x100%" alt="">
						</div>
						<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
						<div class="form-group">
							<label for="caption">Caption</label><br>
							<textarea name="caption" id="caption" cols="100" rows="5"></textarea>
						</div>
						<div>
							<span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="foto" required></span>
							<input type="submit" class="btn btn-primary" name="submit" value="Upload">
						</div>
					</div>
				</form>
			</div>
		</div>
		<?php require_once "sidebar.php"; ?>
	</div>
	<?php require_once "footer.php"; ?>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jasny-bootstrap.js"></script>

</body>

</html>