<?php
require_once "header.php";
require_once "koneksi.php";
?>

<?php
$login = false;
if (isset($_SESSION['nama'])) {
    $login = true;
    if (cek_status($_SESSION['nama']) == 1) {
        $super_user = true;
    }
}
?>
<?php $artikel = tampilkan_komen(); ?>

<?php

if (isset($_GET['search'])) {
    $cari  = $_GET['search'];
    $artikel  = hasil_cari($cari);
}
?>
<div class="wrapper">
    <nav>
        <form action="" method="get">
            <input class="search" type="search" name="search" placeholder="Search......">
        </form>
    </nav>
    <div class="marquee">
        <marquee>
            <p id="teks_berjalan">Destinasi Pariwisata Kota Tangerang Selatan Sudah dikenal para wisatawan luar Kota. Menurut Dinas Pariwisata Kota Tangerang Selatan Mengatakan, wisatawan yang berkunjung ke Tangerang Selatan pada tahun 2019 telah mencapai sekitar 4.082 wisatawan luar provinsi </p>
        </marquee>
    </div>
    <div class="form2">
        <p><a class="btn btn-success" href="komentar.php"> (+) Tambah Komen </a></p>
        <p id="judul_form">Komentar</p>
        <?php while ($row = mysqli_fetch_assoc($artikel)) : ?>
            <div class="artikel2">

                <h2><a href="detail_komentar.php?id=<?= $row['id']; ?>"><?= $row['nama']; ?></a></h2>
                <p id="email"><?= excerpt($row['email']); ?></p>
                <p id="isi">Isi : <?= $row['isi']; ?></p>
                <p id="waktu">Waktu : <?= $row['tanggal']; ?></p>

                <?php if ($login == true) : ?>
                    <a class="btn btn-primary" href="edit_komen.php?id=<?= $row['id']; ?>">Edit</a>

                    <a class="btn btn-danger" href="hapus_komen.php?id=<?= $row['id']; ?>"> Hapus</a>

                <?php endif; ?>
            </div>
        <?php endwhile; ?>
    </div>
    <?php require_once "sidebar.php"; ?>
</div>


<?php require_once "footer.php";
