<?php
require_once "header.php";
require_once "koneksi.php";
$login = false;
if (isset($_SESSION['nama'])) {
    $login = true;
    if (cek_status($_SESSION['nama']) == 1) {
        $super_user = true;
    }
}
$artikel = approval();
if (isset($_GET['search'])) {
    $cari  = $_GET['search'];
    $artikel  = hasil_cari($cari);
}
if (isset($_POST['approve'])) {
    $value = 1;
    $id = $_POST['approve'];
    $statement = status($id, $value);
    if ($statement == true) {
        header('Location:approval.php');
    } else {
        echo "<script>alert('Error!')</script>";
    }
} else if (isset($_POST['reject'])) {
    $value = -1;
    $id = $_POST['reject'];
    $statement = status($id, $value);
    if ($statement == true) {
        header('Location:approval.php');
    } else {
        echo "<script>alert('Error!')</script>";
    }
}
?>
<div class="wrapper">
    <nav>
        <form action="" method="get">
            <input class="search" type="search" name="search" placeholder="Search......">
        </form>
    </nav>
    <div class="marquee">
        <marquee>
            <p id="teks_berjalan">Destinasi Pariwisata Kota Tangerang Selatan Sudah dikenal para wisatawan luar Kota. Menurut Dinas Pariwisata Kota Tangerang Selatan Mengatakan, wisatawan yang berkunjung ke Tangerang Selatan pada tahun 2019 telah mencapai sekitar 4.082 wisatawan luar provinsi </p>
        </marquee>
    </div>
    <div class="form2">
        <p id="judul_form">List Approval Komentar</p>
        <?php while ($row = mysqli_fetch_assoc($artikel)) : ?>
            <div class="artikel2">

                <h2><a href="detail_komentar.php?id=<?= $row['id']; ?>"><?= $row['nama']; ?></a></h2>
                <p id="email"><?= excerpt($row['email']); ?></p>
                <p id="isi">Isi : <?= $row['isi']; ?></p>
                <p id="waktu">Waktu : <?= $row['tanggal']; ?></p>
                <form action="" method="post">

                    <?php if ($login == true) : ?>
                        <button class="btn btn-success" name="approve" value="<?= $row['id']; ?>">Approve</button>

                        <button class="btn btn-danger" name="reject" value="<?= $row['id']; ?>">Reject</button>

                    <?php endif; ?>
                </form>
            </div>
        <?php endwhile; ?>
    </div>
    <?php require_once "sidebar.php"; ?>
</div>


<?php require_once "footer.php";
