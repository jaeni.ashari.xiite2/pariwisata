<?php

require_once "koneksi.php";
require_once "header.php"; ?>

<?php
$error = "";
$id = $_GET['id'];

if (isset($_GET['id'])) {
    $artikel2 = tampilkan_id($id);
    while ($row = mysqli_fetch_assoc($artikel2)) {

        $judul_awal  = $row['judul'];
        $isi_awal    = $row['isi'];
        $tag_awal    = $row['tag'];
        $gambar_awal = $row['gambar'];
    }
}
if (isset($_POST['submit'])) {
    include "includes/config.php";
    $target_dir = "home/";
    $target_file = $target_dir . basename($_FILES["foto"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["foto"]["size"] > 2097152) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if (
        $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif"
    ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
            $nama = basename($_FILES["foto"]["name"]);
            $judul = $_POST['judul'];
            $isi   = htmlspecialchars($_POST['isi']);
            $tag   = $_POST['tag'];
            $query = $koneksi->query("UPDATE artikel SET gambar = '$nama', judul =' $judul', isi = '$isi', tag = '$tag' WHERE id = $id") or die($koneksi->error);
            if (!empty(trim($judul)) && !empty(trim($isi))) {
                if ($query) {
                    $i = $koneksi->insert_id;
                    header("Location: index.php");
                } else {
                    echo '<script>alert("Gagal sob!"); document.location="tambah_data.php?menu=upload";</script>';
                }
            } else {
                $error = "data harus diisi";
            }
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
}

?>

<style media="screen">
    .form_tambah {
        width: 80%;
        height: 900px;
        background: white;
        float: left;
        border-right: 0.5px solid #dddddd;
        box-sizing: border-box;
        padding-top: 20px;
        padding-left: 150px;
    }


    .input {
        width: 400px;
        height: 30px;
        background: #dfdfdf;
        font-size: 18px;
    }

    .isi {
        width: 70%;
        height: 400px;
        background: #dfdfdf;
        font-size: 18px;
    }

    .submit {
        width: 400px;
        height: 30px;
        background: #50a8a9;
        border: none;
        color: white;
        font-size: 18px;
        cursor: pointer;
    }

    .submit:hover {
        background: #249697;
    }

    .error {
        color: red;
    }
</style>
<div class="wrapper">
    <nav>
        <form action="" method="get">
            <input class="search" type="search" name="search" placeholder="Search......">
        </form>
    </nav>
    <div class="marquee">
        <marquee>
            <p id="teks_berjalan">Destinasi Pariwisata Kota Tangerang Selatan Sudah dikenal para wisatawan luar Kota. Menurut Dinas Pariwisata Kota Tangerang Selatan Mengatakan, wisatawan yang berkunjung ke Tangerang Selatan pada tahun 2019 telah mencapai sekitar 4.082 wisatawan luar provinsi </p>
        </marquee>
    </div>
    <div class="form2">
        <div class="container body">
            <h1>Upload Gambar</h1>

            <form method="post" action="" enctype="multipart/form-data">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 700px; height: 150px;">
                        <img data-src="holder.js/100%x100%" src="<?= 'home/' . $gambar_awal ?>" class="img-responsive">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                    <div>
                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="foto" required></span>
                    </div>
                    <label for="judul">Judul</label><br>
                    <input class="input" type="text" name="judul" value="<?= $judul_awal; ?>"><br><br>

                    <label for="isi">Isi</label><br>
                    <textarea class="isi" name="isi" value="" rows="19" cols="100"><?= $isi_awal; ?></textarea><br><br>

                    <label for="tag">Tag</label><br>
                    <input class="input" type="text" name="tag" value="<?= $tag_awal; ?>"><br><br>

                    <div class="error"><br>
                        <?= $error; ?>
                    </div>
                    <br>
                    <input class="submit" type="submit" name="submit" value="Kirim"><br>
                </div>
            </form>
        </div>
    </div>
    <!-- <div class="sidebar"></div>
    <div class="sidebar2"></div> -->
    <?php require_once "footer.php"; ?>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jasny-bootstrap.js"></script>