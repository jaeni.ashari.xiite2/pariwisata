
<?php 
   
    require_once "koneksi.php";
    require_once "header.php";?>

<?php 
$error = "";
    $id = $_GET['id'];

    if(isset($_GET['id'])){
        $artikel2 = tampil_event($id);
        while($row= mysqli_fetch_assoc($artikel2)){
            
        $topic  = $row['topik'];
        $detail    = $row['detail'];
        $terkait    = $row['terkait'];
        }
    }
    if(isset($_POST['submit'])){
        $topic = $_POST['topic'];
        $detail   = $_POST['detail'];
        $terkait   = $_POST['terkait'];
        
        if(!empty(trim($topic)) && !empty(trim($detail))){
            if(edit_event($topic, $detail, $terkait, $id)){
                header('Location: tampil_event.php');
            }else{
                $error = "ada masalah saat edit data";
            }
        }else{
            $error = "data harus diisi";
        }
    }

?>

    <style media="screen">
        
        .form_tambah{
            width: 80%;
            height: 900px;
            background: white;
            float: left;
            border-right: 0.5px solid #dddddd;
            box-sizing: border-box;
            padding-top: 20px;
            padding-left: 150px;
}
        
        
        .input{
            width: 400px;
            height: 30px;
            background: #dfdfdf;
            font-size: 18px;
        }
        .isi{
            width: 70%;
            height: 400px;
            background: #dfdfdf;
            font-size: 18px;
        }
        .submit{
            width: 400px;
            height: 30px;
            background: #50a8a9;
            border: none;
            color: white;
            font-size: 18px;
            cursor: pointer;
        }
        .submit:hover{
            background: #249697;
        }
        .error{
            color: red;
        }
    </style>
<div class="wrapper">
    <div class="form_tambah">
        <form action="" method="post">

            <label for="judul">Topic</label><br>
            <input class="input" type="text" name="topic" value="<?=$topic;?>"><br><br>

            <label for="isi">Detail</label><br>
            <textarea class="isi" name="detail" value="<?=$detail;?>" rows="19" cols="100"></textarea><br><br>

            <label for="judul">Terkait</label><br>
            <input class="input" type="text" name="terkait" value="<?=$terkait;?>"><br><br>


            <div class="error"><br>
                <?= $error; ?>
            </div>
            <br>
            <input class="submit" type="submit" name="submit" value="Kirim"><br>


        </form>
    </div>
    <div class="sidebar"></div>
    <div class="sidebar2"></div>
</div>
<?php require_once "footer.php"; ?>
