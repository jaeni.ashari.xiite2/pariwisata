<?php
//sumber w3schools.com
include("includes/config.php");
$folder = "gallery/";
$upload_image = $_FILES["foto"]["name"];
$width_size = 470;
$target_file = $folder . $upload_image;

$file = $_FILES['foto']['tmp_name'];
$sourceProperties = getimagesize($file);
$imageType = $sourceProperties[2];

$uploadOk = 1;
$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if (isset($_POST["submit"])) {
	// Check if file already exists
	if (file_exists($target_file)) {
		echo "Sorry, file already exists.";
		$uploadOk = 0;
	}
	// Check file size
	if ($_FILES["foto"]["size"] > 10097152) {
		echo "Sorry, your file is too large.";
		$uploadOk = 0;
	}
	// Allow certain file formats
	if (
		$imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif"
	) {
		echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		echo "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
			$resize_image = $folder . "resize_" . $upload_image;
			list($width, $height) = getimagesize($target_file);
			$k = $width / $width_size;
			$newwidth = $width / $k;
			$newheight = $height / $k;

			switch ($imageType) {
				case IMAGETYPE_PNG:
					$thumb = imagecreatetruecolor($newwidth, $newheight);
					$source = imagecreatefrompng($target_file);
					imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
					imagepng($thumb, $resize_image);
					imagedestroy($thumb);
					imagedestroy($source);
					break;

				case IMAGETYPE_GIF:
					$thumb = imagecreatetruecolor($newwidth, $newheight);
					$source = imagecreatefromgif($target_file);
					imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
					imagegif($thumb, $resize_image);
					imagedestroy($thumb);
					imagedestroy($source);
					break;

				case IMAGETYPE_JPEG:
					$thumb = imagecreatetruecolor($newwidth, $newheight);
					$source = imagecreatefromjpeg($target_file);
					imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
					imagejpeg($thumb, $resize_image);
					imagedestroy($thumb);
					imagedestroy($source);
					break;

				default:
					echo "Invalid Image type.";
					exit;
					break;
			}

			$nama = "resize_" . $upload_image;
			$caption = $_POST["caption"];
			$tgl = date("Y-m-d");
			$query = $koneksi->query("INSERT INTO galeri VALUES(null, '$tgl','$nama', '$caption')") or die($koneksi->error);
			if ($query) {
				$i = $koneksi->insert_id;
				header("Location: foto.php?id=" . $i);
			} else {
				echo '<script>alert("Gagal sob!"); document.location="upload.php?menu=upload";</script>';
			}
		} else {
			echo "Sorry, there was an error uploading your file.";
		}
	}
}
