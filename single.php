<?php require_once "header.php";
require_once "koneksi.php";

$id = $_GET['id'];

if (isset($_GET['id'])) {
    $artikel2 = tampilkan_id($id);

    while ($row = mysqli_fetch_assoc($artikel2)) {
        $judul_awal = $row['judul'];
        $isi_awal   = $row['isi'];
        $tag_awal   = $row['tag'];
    }
}


?>
<style media="screen">
    .form_tambah {
        width: 80%;
        height: 800px;
        background: white;
        float: left;
        padding: 30px;
        box-sizing: border-box;
        border-bottom: 0.5px solid #dddddd;
    }

    #judul_single {
        font-size: 25px;
        font-weight: bold;
    }

    #isi_single {
        font-size: 15px;
        margin-top: 20px;
    }

    #tag_awal {
        font-size: 12px;
    }
</style>

<div class="wrapper">
    <nav>
        <form action="" method="get">
            <input class="search" type="search" name="search" placeholder="Search......">
        </form>
    </nav>
    <div class="marquee">
        <marquee>
            <p id="teks_berjalan">Destinasi Pariwisata Kota Tangerang Selatan Sudah dikenal para wisatawan luar Kota. Menurut Dinas Pariwisata Kota Tangerang Selatan Mengatakan, wisatawan yang berkunjung ke Tangerang Selatan pada tahun 2019 telah mencapai sekitar 4.082 wisatawan luar provinsi </p>
        </marquee>
    </div>
    <div class="form2">
        <div class="container body">
            <div class="page-header">
                <p id="judul_single"><?= $judul_awal; ?></p><br>
            </div>
            <?php
            $id = $_GET['id'];
            $query = $koneksi->query("SELECT * FROM artikel WHERE id='$id'") or die($koneksi->error);
            if ($query->num_rows) {
                $row = $query->fetch_assoc();
                echo '
			<div class="row">
				<div class="col-md-8">
					<img src="home/' . $row['gambar'] . '" class="img-responsive">
				</div>
			</div>
			';
            } else {
                echo '404 Not Found!';
            }
            ?>
            <div>
                <p id="isi_single"><?= $isi_awal; ?></p><br>
                <p id="tag_awal"><?= $tag_awal; ?></p><br>
            </div>
        </div>
    </div>
    <?php require_once "sidebar.php"; ?>
</div>
<?php require_once "footer.php"; ?>