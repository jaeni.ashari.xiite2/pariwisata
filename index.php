<?php
require_once "header.php";
require_once "koneksi.php";
?>

<?php
$login = false;
if (isset($_SESSION['nama'])) {
    $login = true;
    if (cek_status($_SESSION['nama']) == 1) {
        $super_user = true;
    }
}
?>
<?php $artikel = tampilkan(); ?>

<?php

if (isset($_GET['search'])) {
    $cari  = $_GET['search'];
    $artikel  = hasil_cari($cari);
}
?>
<div class="wrapper">
    <nav>
        <form action="" method="get">
            <input class="search" type="search" name="search" placeholder="Search......">
        </form>
    </nav>
    <div class="marquee">
        <marquee>
            <p id="teks_berjalan">Destinasi Pariwisata Kota Tangerang Selatan Sudah dikenal para wisatawan luar Kota. Menurut Dinas Pariwisata Kota Tangerang Selatan Mengatakan, wisatawan yang berkunjung ke Tangerang Selatan pada tahun 2019 telah mencapai sekitar 4.082 wisatawan luar provinsi </p>
        </marquee>
    </div>
    <div class="form2">
        <p id="judul_form">Artikel</p>
        <div class="container body">
            <?php while ($row = mysqli_fetch_assoc($artikel)) : ?>
                <div class="artikel2">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="<?= 'home/' . $row['gambar'] ?>" class="img-responsive">
                        </div>
                    </div>
                    <h2><a href="single.php?id=<?= $row['id']; ?>"><?= $row['judul']; ?></a></h2>
                    <p id="isi"><?= excerpt($row['isi']); ?></p>
                    <p id="waktu">Waktu : <?= $row['waktu']; ?></p>
                    <p id="tag">Tag : <?= $row['tag']; ?></p>

                    <?php if ($login == true) : ?>
                        <a class="btn btn-primary" href="edit.php?id=<?= $row['id']; ?>">Edit</a>

                        <a class="btn btn-danger" href="hapus.php?id=<?= $row['id']; ?>"> Hapus</a>

                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php require_once "sidebar.php"; ?>
</div>
<?php require_once "footer.php";
